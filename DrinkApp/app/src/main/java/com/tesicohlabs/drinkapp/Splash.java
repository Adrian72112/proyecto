package com.tesicohlabs.drinkapp;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;



public class Splash extends AppCompatActivity {
    DrinkAppBD bd;
    Typeface typeface;
    Animation animation;
    TextView texto1;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        View decorView = getWindow().getDecorView();

        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.

        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);




        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(2025);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{

                    Cursor c = db.rawQuery(" SELECT Informacion FROM Persona", null);

                    if (c.moveToFirst()) {
                        //Aqui va el codigo para entrar a la otra ventana
                        Intent intent = new Intent(Splash.this, datos2.class);
                        startActivity(intent);
                    } else {
                        //Aqui va el codigo para entrar a la otra ventana
                        Intent intent = new Intent(Splash.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
            }
        };

        fuente2();

        //Creamos la base de datos si no esta creada
        bd = new DrinkAppBD(this,"DrinkAppBD",null,1);

        db = bd.getWritableDatabase();

        timerThread.start();

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

    private void fuente2(){
        // Cambiamos el texto por defecto del texto 1
        texto1 = (TextView)findViewById(R.id.textView);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/data.ttf");
        texto1.setTypeface(typeface);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.clockwise);
        ImageView logo = (ImageView)findViewById(R.id.imageView);
        logo.startAnimation(animation);

    }

}
