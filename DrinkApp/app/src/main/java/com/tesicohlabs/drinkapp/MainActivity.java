package com.tesicohlabs.drinkapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.dd.morphingbutton.MorphingButton;
import com.vstechlab.easyfonts.EasyFonts;

public class MainActivity extends AppCompatActivity {
    Typeface typeface;
    TextView titulo;
    TextView adver4;
    TextView adver5;
    TextView adver6;
    TextView adver7;
    TextView adver8;
    Button boton;
    Animation animation;
    MorphingButton btnMorph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnMorph = (MorphingButton) findViewById(R.id.btnMorph1);

        //Cambiamos la fuente del textview del titulo
        fuente();
        //cambiamos la fuente del textview de las advertencias
        fuente2();

        // Animomaos la imagen del warning
        animation();


    }

    public void sendMessage(View view){

        MorphingButton.Params circle = MorphingButton.Params.create()
                .duration(400)
                .cornerRadius(120) // 56 dp
                .width(190) // 56 dp
                .height(190)
        .icon(R.drawable.background_checkbox_on); // icon
        btnMorph.morph(circle);




        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(410);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    //Cuando se apreta el boton se abre la nueva ventana
                    Intent intent = new Intent(MainActivity.this, Datos.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        timerThread.start();


    }


    public void fuente(){
        //Instanciamos textview el titulo
        titulo = (TextView)findViewById(R.id.textView3);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/warning.ttf");
        titulo.setTypeface(typeface);

    }

    public void fuente2(){
        adver4 = (TextView)findViewById(R.id.textView4);
        adver5 = (TextView)findViewById(R.id.textView5);
        adver6 = (TextView)findViewById(R.id.textView6);
        adver7 = (TextView)findViewById(R.id.textView7);
        adver8 = (TextView)findViewById(R.id.textView8);
        adver4.setTypeface(EasyFonts.robotoRegular(this));
        adver5.setTypeface(EasyFonts.robotoRegular(this));
        adver6.setTypeface(EasyFonts.robotoRegular(this));
        adver7.setTypeface(EasyFonts.robotoRegular(this));
        adver8.setTypeface(EasyFonts.robotoRegular(this));
    }

    public void animation(){
        ImageView warning = (ImageView)findViewById(R.id.imageView2);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
        warning.startAnimation(animation);
    }

}
