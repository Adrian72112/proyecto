package com.tesicohlabs.drinkapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vstechlab.easyfonts.EasyFonts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Tab2 extends Fragment {
    DrinkAppBD bd;
    SQLiteDatabase db;
    TextView titulo;
    View v;
    LinearLayout linearCont;
    LinearLayout layoutDef;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.tab_2,container,false);
        fuente();

        bd = new DrinkAppBD(getActivity(),"DrinkAppBD",null,1);
        db = bd.getWritableDatabase();

        Date actualDate = new Date();
        DateFormat hourFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar ca = Calendar.getInstance();
        ca.setTime(actualDate);
        ca.add(Calendar.DATE,-1);
        actualDate = ca.getTime();

        Date actualDate2 = new Date();
        DateFormat hourFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        String date2 = hourFormat2.format(actualDate2);


       Cursor c = db.rawQuery("SELECT * FROM Consume" +
                             " WHERE Fecha = '"+hourFormat.format(actualDate)+"'" +
                                " OR Fecha = '"+date2+"';", null);

        //Creamo la view programatica a huevo
      if (c.moveToFirst()){
                int j =0;
          do{
              //Arranca la tortu

              ImageView imagenBeb = new ImageView(getActivity());
              imagenBeb.setImageDrawable(getResources().getDrawable(R.drawable.tab2bebida));
              imagenBeb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

              TextView nombreBebida = new TextView(getActivity());
              nombreBebida.setTypeface(EasyFonts.robotoBold(getContext()));
              nombreBebida.setTextSize(30);
              nombreBebida.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
              nombreBebida.setText(c.getString(1));
              Cursor c2 = db.rawQuery("SELECT * FROM Bebida WHERE Nombre='"+c.getString(1)+"';",null);
              if(c2.moveToFirst()) {
                  nombreBebida.setText(c.getString(1) + "\n % Alc: " + c2.getString(1));
              }
              c2.close();

              //Datos more inteligentes aqui

              linearCont = new LinearLayout(getActivity());
              linearCont.setPadding(0,10,0,10);
              //linearCont.setBackgroundColor(getResources().getColor(R.color.light_blue));
              linearCont.setId(j);
              // specifying vertical orientation
              linearCont.setOrientation(LinearLayout.HORIZONTAL);
              linearCont.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
              // creating LayoutParams
              //ViewGroup.LayoutParams linLayoutParam = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

              layoutDef = (LinearLayout) v.findViewById(R.id.tab2Linear);
              layoutDef.addView(linearCont);
              linearCont.addView(imagenBeb);
              linearCont.addView(nombreBebida);
                j++;
          }while(c.moveToNext());
        }


        //-------------Boton Refresh-------------------

        FloatingActionButton btnRefresh = (FloatingActionButton)v.findViewById(R.id.btnAceptar25);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //layoutDef.setVisibility(View.GONE);

                Date actualDate = new Date();
                DateFormat hourFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar ca = Calendar.getInstance();
                ca.setTime(actualDate);
                ca.add(Calendar.DATE,-1);
                actualDate = ca.getTime();

                Date actualDate2 = new Date();
                DateFormat hourFormat2 = new SimpleDateFormat("yyyy-MM-dd");
                String date2 = hourFormat2.format(actualDate2);


                Cursor c = db.rawQuery("SELECT * FROM Consume" +
                        " WHERE Fecha = '"+hourFormat.format(actualDate)+"'" +
                        " OR Fecha = '"+date2+"';", null);

                linearCont.setVisibility(View.GONE);

                //Creamo la view programatica a huevo
                if (c.moveToFirst()){
                    int j =0;
                    do{
                        //Arranca la tortu

                        ImageView imagenBeb = new ImageView(getActivity());
                        imagenBeb.setImageDrawable(getResources().getDrawable(R.drawable.tab2bebida));
                        imagenBeb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                        TextView nombreBebida = new TextView(getActivity());
                        nombreBebida.setTypeface(EasyFonts.robotoBold(getContext()));
                        nombreBebida.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        nombreBebida.setTextSize(30);
                        nombreBebida.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                        nombreBebida.setText(c.getString(1));
                        Cursor c2 = db.rawQuery("SELECT * FROM Bebida WHERE Nombre='"+c.getString(1)+"';",null);
                        if(c2.moveToFirst()) {
                            nombreBebida.setText(c.getString(1) + "\n % Alc: " + c2.getString(1));
                        }
                        c2.close();

                        //Datos more inteligentes aqui

                        linearCont = new LinearLayout(getActivity());
                        linearCont.setPadding(0,10,0,10);
                        //linearCont.setBackgroundColor(getResources().getColor(R.color.light_blue));
                        linearCont.setId(j);
                        // specifying vertical orientation
                        linearCont.setOrientation(LinearLayout.HORIZONTAL);
                        linearCont.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        // creating LayoutParams
                        //ViewGroup.LayoutParams linLayoutParam = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        LinearLayout layoutDef = (LinearLayout) getActivity().findViewById(R.id.tab2Linear);
                        layoutDef.addView(linearCont);
                        linearCont.addView(imagenBeb);
                        linearCont.addView(nombreBebida);
                        j++;
                    }while(c.moveToNext());
                }

            }
        });



        return v;
    }

    public void fuente(){
        //Instanciamos textview el titulo
        titulo = (TextView)v.findViewById(R.id.textView11);
        titulo.setTypeface(EasyFonts.captureIt(getActivity()));

    }

}
