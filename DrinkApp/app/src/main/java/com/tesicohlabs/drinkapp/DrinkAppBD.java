package com.tesicohlabs.drinkapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DrinkAppBD extends SQLiteOpenHelper {
    //Sentencia sql para crear la tabla Persona
    String sql1 = "CREATE TABLE Persona (Nombre varchar(15) primary key not null," +
                              "Edad integer not null," +
                              "Peso integer not null," +
                              "Informacion boolean not null," +
                              "TomFreq boolean not null," +
                              "SiComio boolean," +
                              "Medicamentos boolean," +
                              "Sexo varchar(10) not null);";

    //Sentencia sql para crear la tabla Bebida
    String sql2 = "CREATE TABLE Bebida (Nombre varchar(50) primary key not null," +
                            "PorcAlc integer not null);";

    //Sentencia sql para crear la tabla Consume relacionada con Persona y Bebida
    String sql3 = "CREATE TABLE Consume (NombrePer varchar(15) not null," +
                            "NombreBebida varchar(50) not null," +
                            "Fecha date not null," +
                            "FOREIGN KEY(NombrePer) REFERENCES Persona(Nombre));";

    String sql4 = "CREATE TABLE medicamentos (" +
                    "nombre varchar(50) not null primary key);";


    //Sentencia para crear datos iniciales
    String insert1 = "INSERT INTO Bebida (Nombre, PorcAlc) values ('Vino',12)";
    
    String insert3 = "INSERT INTO Bebida (Nombre, PorcAlc) VALUES ('Cocoroco',94) ,('Chinchón Conga',94)" +
            ",('Absenta',77)" +
            ",('Chinchón Seco',74)" +
            ",('Mezcal',55)" +
            ",('Ginebra',52)" +
            ",('Güisqui',51)" +
            ",('Orujo Blanco',50)" +
            ",('Cachaza',46)" +
            ",('Aguardiente',44)" +
            ",('Caña',44)" +
            ",('Grappa',44)" +
            ",('Pisco',41)" +
            ",('Tequila',41)" +
            ",('Bourbon',41)" +
            ",('Ron',40)" +
            ",('Coñac',40)" +
            ",('Vodka',41)" +
            ",('Aquavit',39)" +
            ",('Fernet',39)" +
            ",('Brandy',38)" +
            ",('Becherovka',38)" +
            ",('Gin',38)" +
            ",('Chinchón',37)" +
            ",('Jäggermeister',35)" +
            ",('Tía María',31)" +
            ",('Palo',30)" +
            ",('Punsch',26)" +
            ",('Cherry Heering',25)" +
            ",('Pacharán',25)" +
            ",('Vino de arroz',21)" +
            ",('Oporto',20)" +
            ",('Vermouth',19)" +
            ",('Jerez',17)" +
            ",('Pelin',8)" +
            ",('Pulque',8)" +
            ",('Cerveza',7)" +
            ",('Sidra',5);";

    String insert2 = "INSERT INTO medicamentos (nombre) VALUES ('Nizatidine') " +
            "            ,('Metoclopramide') " +
            "            ,('Cimetidine') " +
            "            ,('Ranitidine') " +
            "            ,('Loratadine') " +
            "            ,('Hydroxyzine') " +
            "            ,('Diphenhydramine') " +
            "            ,('Desloratadine') " +
            "            ,('Brompheniramine') " +
            "            ,('Chlorpheniramine') " +
            "            ,('Cetirizine') " +
            "            ,('Isosorbide') " +
            "            ,('Nitroglycerin') " +
            "            ,('Lorazepam') " +
            "            ,('Buspirone') " +
            "            ,('Clonazepam') " +
            "            ,('Chlordiazepoxide') " +
            "            ,('Paroxetine') " +
            "            ,('Diazepam') " +
            "            ,('Alprazolam') " +
            "            ,('Celecoxib') " +
            "            ,('Naproxen') " +
            "            ,('Diclofenac') " +
            "            ,('Amphetamine/dextro-amphetamine') " +
            "            ,('Methylphenidate') " +
            "            ,('Dextroamphetamine') " +
            "            ,('Atomoxetine') " +
            "            ,('Lisdexamfetamine') " +
            "            ,('Warfarin') " +
            "            ,('Lovastatin  Niacin') " +
            "            ,('Lovastatin') " +
            "            ,('Rosuvastatin') " +
            "            ,('Atorvastatin') " +
            "            ,('Niacin') " +
            "            ,('Pravastatin') " +
            "            ,('Pravastatin  Aspirin') " +
            "            ,('Ezetimibe  Simvastatin') " +
            "            ,('Simvastatin') " +
            "            ,('Phenytoin') " +
            "            ,('Gabapentin') " +
            "            ,('Levetiracetam') " +
            "            ,('Phenobarbital') " +
            "            ,('Lamotrigine') " +
            "            ,('Pregabalin') " +
            "            ,('Carbamazepine') " +
            "            ,('Topiramate') " +
            "            ,('Oxcarbazepine') " +
            "            ,('Barbiturates') " +
            "            ,('Aripriprazone') " +
            "            ,('Clomipramine') " +
            "            ,('Citalopram') " +
            "            ,('Clozapine') " +
            "            ,('Duloxetine') " +
            "            ,('Trazodone') " +
            "            ,('Venlafaxine') " +
            "            ,('Amitriptyline') " +
            "            ,('Ziprasidone') " +
            "            ,('Paliperidone') " +
            "            ,('Escitalopram') " +
            "            ,('Fluvoxamine') " +
            "            ,('Phenelzine') " +
            "            ,('Desipramine') " +
            "            ,('Tranylcypromine') " +
            "            ,('Desevenlafaxine') " +
            "            ,('Fluoxetine') " +
            "            ,('Mirtazapine') " +
            "            ,('Risperidone') " +
            "            ,('Quetiapine') " +
            "            ,('Nefazodone') " +
            "            ,('Fluoxetine/Olanzapine') " +
            "            ,('Bupropion') " +
            "            ,('Sertraline') " +
            "            ,('Olanzapine') " +
            "            ,('Chlorpropamide') " +
            "            ,('Glipizide') " +
            "            ,('Metformin') " +
            "            ,('Glyburide') " +
            "            ,('Tolbutamide') " +
            "            ,('Tolazamide') " +
            "            ,('Ibuprofen') " +
            "            ,('Aspirin, Acetaminophen') " +
            "            ,('Acetaminophen') " +
            "            ,('Cyclobenzaprine') " +
            "            ,('Carisoprodol') " +
            "            ,('Propoxyphene') " +
            "            ,('Merepidine') " +
            "            ,('Butalbital  codeine') " +
            "            ,('Oxycodone') " +
            "            ,('Hydrocodone') " +
            "            ,('Valproic acid') " +
            "            ,('Lithium') " +
            "            ,('Nitrofurantoin') " +
            "            ,('Metronidazole') " +
            "            ,('Griseofulvin') " +
            "            ,('Ketoconazole') " +
            "            ,('Isoniazid') " +
            "            ,('Cycloserine') " +
            "            ,('Tinidazole') " +
            "            ,('Azithromycin') " +
            "            ,('Doxazosin') " +
            "            ,('Tamsulosin') " +
            "            ,('Terazosin') " +
            "            ,('Prazosin') " +
            "            ,('Meclizine') " +
            "            ,('Dimenhydrinate') " +
            "            ,('Promethazine') " +
            "            ,('Quinapril') " +
            "            ,('Verapamil') " +
            "            ,('Hydrochlorothiazide') " +
            "            ,('Clonidine') " +
            "            ,('Losartan') " +
            "            ,('Benzapril') " +
            "            ,('Amlodipine mesylate') " +
            "            ,('Lisinopril') " +
            "            ,('Enalapril') " +
            "            ,('Zolpidem') " +
            "            ,('Eszopiclone') " +
            "            ,('Estazolam') " +
            "            ,('Temazepam') " +
            "            ,('Doxylamine') " +
            "            ,('Dextromethorpan') " +
            "            ,('Guaifenesin  codeine');";

    public DrinkAppBD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Creamos la tabla Persona
        db.execSQL(sql1);

        //Creamos la tabla Bebida
        db.execSQL(sql2);

        //Creamos la tabla Consume
        db.execSQL(sql3);

        db.execSQL(sql4);

        //Insertamos datos iniciales a la tabla
        datosIniciales(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void datosIniciales(SQLiteDatabase db){
        db.execSQL(insert1);
        db.execSQL(insert2);
        db.execSQL(insert3);
    }
}
