package com.tesicohlabs.drinkapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.vstechlab.easyfonts.EasyFonts;
import java.util.regex.Pattern;

public class Datos extends AppCompatActivity {

    Boolean existe = false;
    String nombre2;
    String edad2;
    String peso2;
    String TomFreq2;
    String sexo2;

    String nombre;
    String edad;
    int peso;
    CheckBox tomF;
    boolean tomFre = false;
    String sexo;
    DrinkAppBD bd;
    SQLiteDatabase db;


    TextView titulo1;
    RadioGroup rdGroup;
    private TextInputLayout tilNombre;
    private TextInputLayout tilEdad;
    private TextInputLayout tilPeso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);

        RelativeLayout linear = (RelativeLayout) findViewById(R.id.datosLayout);

        YoYo.with(Techniques.RotateIn)
                .duration(500)
                .playOn(linear);


        //Creamos la base de datos si no esta creada
        bd = new DrinkAppBD(this,"DrinkAppBD",null,1);
        db = bd.getWritableDatabase();
        checkRegistro();

        // Referencias TILs
        tilNombre = (TextInputLayout) findViewById(R.id.til_nombre);
        tilEdad = (TextInputLayout) findViewById(R.id.til_Edad);
        tilPeso = (TextInputLayout) findViewById(R.id.til_Peso);

        //Animations y fuente del titulo
        tituloFont();
        animations();

    }

    public void animations(){

        CardView view = (CardView)findViewById(R.id.view);
        CardView view2 = (CardView)findViewById(R.id.view2);

        YoYo.with(Techniques.ZoomIn)
                .duration(600)
                .playOn(view);

        YoYo.with(Techniques.ZoomIn)
                .duration(600)
                .playOn(view2);
    }

    //Cuando apretamos el boton cancelar cerramos la pestaña
    public void btnCancelar(View view){
        finish();
    }

    // Cuando apretamos el boton aceptar
    public void btnAceptar(View view){
        validarDatos();
    }


    public void tituloFont(){
        titulo1 = (TextView)findViewById(R.id.textView9);
        titulo1.setTypeface(EasyFonts.captureIt(this));

        YoYo.with(Techniques.RubberBand)
                .duration(1200)
                .playOn(titulo1);


    }

    private boolean esNombreValido(String nombre){
        Pattern patron = Pattern.compile("^[a-zA-Z ]+$");
        if(!patron.matcher(nombre).matches() || nombre.length() > 30){
            tilNombre.setError("Nombre inválido!");
            YoYo.with(Techniques.Shake)
                    .duration(500)
                    .playOn(tilNombre);
            return false;
        }else{
            tilNombre.setError(null);
        }
        return true;
    }

    private boolean esEdadValido(String Edad){
        if(Edad.length() > 2 || Edad.isEmpty()){
            tilEdad.setError("Edad incorrecta!");
            YoYo.with(Techniques.Shake)
                    .duration(500)
                    .playOn(tilEdad);
            return false;
        } else {
            tilEdad.setError(null);
        }
        return true;
    }

    private boolean esPesoValido(int Peso){
        if(Peso > 450 | Peso==0){
                tilPeso.setError("Peso incorrecto o nulo!");
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .playOn(tilPeso);
                return false;
        }else{
            tilPeso.setError(null);
        }
        return true;
    }


    private void validarDatos() {
        nombre = tilNombre.getEditText().getText().toString();
        edad = tilEdad.getEditText().getText().toString();

        //peso = 250;
        try{
            peso = Integer.parseInt(tilPeso.getEditText().getText().toString());
        }catch (NumberFormatException nfe){
            tilPeso.setError("Dato incorrecto!");
        }
        rdGroup = (RadioGroup)findViewById(R.id.GrbGrupo1);

        boolean a = esNombreValido(nombre);
        boolean b = esEdadValido(edad);
        boolean c = esPesoValido(peso);

        if (a && b && c && rdGroup.getCheckedRadioButtonId() > 0) {
            // OK, se pasa a la siguiente acción

            //Check box checkeado
            tomF = (CheckBox)findViewById(R.id.checkBox);
            if (tomF.isChecked()){
                tomFre = true;
            }else{
                tomFre  = false;
            }

            RadioButton masc = (RadioButton)findViewById(R.id.radioButton);

            if(masc.isChecked()){
                sexo = "Masculino";
            }else{
                sexo = "Femenino";
            }

            if(Integer.parseInt(edad) < 18){
                Toast.makeText(this,"ADVERTENCIA!\nUsted es menor de edad\nLa app no se hace responsable del mal uso que esta tenga",Toast.LENGTH_LONG).show();
            }


            insertDatos(tomFre);

        } else {
            Toast.makeText(this, "Datos incompletos", Toast.LENGTH_SHORT).show();
        }

    }



    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();


        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButton2:
                if (checked)
                    sexo = "Femenino";
                    break;
            case R.id.radioButton:
                if (checked)
                    sexo = "Masculino";
                    break;
        }
    }

    private void insertDatos(Boolean tomF){
        DrinkAppBD bd = new DrinkAppBD(this, "DrinkAppBD" , null, 1);
        SQLiteDatabase db = bd.getWritableDatabase();
        Cursor c = db.rawQuery(" SELECT * FROM Persona", null);

        if (c.moveToFirst()) {
            String tomFr = tomF.toString();
            db.execSQL("Update Persona Set Edad='"+edad+"'"+", Peso='"+peso+"'"+", TomFreq='"+tomFr+"'"+", Sexo='"+sexo+"'"+" "+"where Nombre='"+nombre+"'");
            Toast.makeText(this, "Modificación exitosa!", Toast.LENGTH_SHORT).show();
            db.close();
            finish();

        }else {
            //Creamos el registro a insertar como objeto ContentValues
            ContentValues nuevoRegistro = new ContentValues();
            nuevoRegistro.put("Nombre", nombre);
            nuevoRegistro.put("Edad", Integer.parseInt(edad));
            nuevoRegistro.put("Peso", peso);
            nuevoRegistro.put("Informacion", true);
            nuevoRegistro.put("TomFreq", tomF);
            nuevoRegistro.put("SiComio", false);
            nuevoRegistro.put("Medicamentos", false);
            nuevoRegistro.put("Sexo", sexo);
            db.insert("Persona", null, nuevoRegistro);
            db.close();
            Toast.makeText(this, "Se guarda el registro", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, datos2.class);
            startActivity(intent);
            finish();
        }

    }

    private void checkRegistro(){
        Cursor c = db.rawQuery(" SELECT * FROM Persona", null);

        if (c.moveToFirst()) {
            //Si existe registro agarramos y guardamos los datos para mostrarlos de nuevo
            nombre2= c.getString(0);
            edad2 = c.getString(1);
            peso2= c.getString(2);
            TomFreq2 = c.getString(4);
            sexo2= c.getString(7);
            existe = true;

            //Ingresamos todos los registros en los diferentes campos

            TextView titulo = (TextView)findViewById(R.id.textView9);
            titulo.setPadding(5,0,0,0);
            titulo.setText("SET");

            Button boton = (Button)findViewById(R.id.btnAceptar);
            boton.setText("Modificar");

            EditText nombre = (EditText)findViewById(R.id.campo_nombre);
            nombre.setText(nombre2);
            nombre.setKeyListener(null);

            EditText edad = (EditText)findViewById(R.id.txtEdad);
            edad.setText(edad2, TextView.BufferType.EDITABLE);

            EditText peso = (EditText)findViewById(R.id.txtPeso);
            peso.setText(peso2, TextView.BufferType.EDITABLE);

            RadioGroup grp = (RadioGroup)findViewById(R.id.GrbGrupo1);

            RadioButton Masc = (RadioButton)findViewById(R.id.radioButton);
            RadioButton Fem = (RadioButton)findViewById(R.id.radioButton2);

            if (sexo2.equals("Femenino")) {
                grp.check(R.id.radioButton2);
            }else{
                grp.check(R.id.radioButton);
            }

            CheckBox box = (CheckBox)findViewById(R.id.checkBox);

            if (TomFreq2.equals("false")){
                box.setChecked(false);
            }else{
                box.setChecked(true);
            }


        }

        db.close();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }


}
