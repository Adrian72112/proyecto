package com.tesicohlabs.drinkapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.vstechlab.easyfonts.EasyFonts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class Tab1 extends Fragment{
    View v;
    TextView titulo;
    //Array
    DrinkAppBD bd;
    SQLiteDatabase db;
    String[] stringItems;
    int j;
    ImageButton btnTag;

    String nombre;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v =inflater.inflate(R.layout.tab_1,container,false);
        fuente();
        j=0;

        YoYo.with(Techniques.Tada)
                .duration(700)
                .playOn(v.findViewById(R.id.textViewTitulo));

        final ArrayList<String> bebidasSeleccionadas = new ArrayList<>();

        FloatingActionButton agregarBebida = (FloatingActionButton)v.findViewById(R.id.btnAceptar24);
        bd = new DrinkAppBD(getActivity(),"DrinkAppBD",null,1);
        db = bd.getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM Persona",null);

        if(c.moveToFirst()){
            nombre = c.getString(0);
        }
        c.close();



        agregarBebida.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Cursor c = db.rawQuery("SELECT * FROM Bebida ORDER BY Nombre ASC", null);
                        stringItems = new String[39];
                        int i =0;


                        if (c.moveToFirst()) {
                            //Recorremos el cursor hasta que no haya más registros
                            do {
                                stringItems[i] = c.getString(0);
                                i++;
                            } while(c.moveToNext());
                        }
                        c.close();
                        final ActionSheetDialog dialog = new ActionSheetDialog(getActivity(), stringItems, null);
                        dialog.title("Bebidas  Alcoholicas")//
                                .titleTextSize_SP(14.5f)//
                                .cancelText("Cerrar")
                                .show();

                        dialog.setOnOperItemClickL(new OnOperItemClickL() {

                            @Override
                            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                                //stringItems[position]; para obtener el nombre de la bebida seleccionada
                               // Toast.makeText(getActivity(), stringItems[position], Toast.LENGTH_SHORT).show();

                                if (j < 5) {
                                    bebidasSeleccionadas.add(j,stringItems[position]);


                                    //-------------------------------------------------------------


                                    Date date = new Date();
                                    DateFormat hourFormat = new SimpleDateFormat("yyyy-MM-dd");

                                    db.execSQL("INSERT INTO Consume(NombrePer, NombreBebida, Fecha)" +
                                            " VALUES ('" + nombre + "','" + stringItems[position] + "','" + hourFormat.format(date) + "');");



                                    //Guardado en la base de daots-------------


                                    //Toast.makeText(getActivity(), bebidasSeleccionadas.get(j), Toast.LENGTH_SHORT).show();
                                    btnTag = new ImageButton(getActivity());
                                    btnTag.setImageDrawable(getResources().getDrawable(R.drawable.bebida));
                                    btnTag.setBackgroundColor(getResources().getColor(R.color.light_blue));
                                    btnTag.setId(j);

                                    TextView boteTxt = new TextView(getActivity());

                                    if (stringItems[position].length() > 4) {
                                        btnTag.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                                        boteTxt.setPadding(15,0,15,0);
                                    }else{
                                        btnTag.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                                        boteTxt.setPadding(15, 0, 15, 0);
                                    }
                                    boteTxt.setBackgroundColor(getResources().getColor(R.color.light_blue));
                                    boteTxt.setTextSize(20);
                                    boteTxt.setTypeface(EasyFonts.robotoBold(getContext()));
                                    boteTxt.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                                    boteTxt.setGravity(Gravity.CENTER);
                                    boteTxt.setText(stringItems[position]);
                                    LinearLayout bebidas = (LinearLayout) getActivity().findViewById(R.id.bebidasLinear);
                                    // creating LinearLayout
                                    final LinearLayout linLayout = new LinearLayout(getActivity());
                                    linLayout.setId(j);
                                    linLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                                    linLayout.setDividerDrawable(getResources().getDrawable(R.drawable.divider));
                                    // specifying vertical orientation
                                    linLayout.setOrientation(LinearLayout.VERTICAL);
                                    // creating LayoutParams
                                    LayoutParams linLayoutParam = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                                    bebidas.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE | LinearLayout.SHOW_DIVIDER_BEGINNING | LinearLayout.SHOW_DIVIDER_END);
                                    bebidas.addView(linLayout, linLayoutParam);
                                    linLayout.addView(boteTxt);
                                    linLayout.addView(btnTag);
                                    j++;

                                    btnTag.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Toast.makeText(getActivity(), bebidasSeleccionadas.get(v.getId())+"  Eliminado", Toast.LENGTH_SHORT).show();
                                            linLayout.setVisibility(View.GONE);
                                            j--;
                                            db.execSQL("DELETE FROM Consume WHERE NombreBebida='"+bebidasSeleccionadas.get(v.getId())+"';");


                                        }
                                    });
                                }else {
                                    Toast.makeText(getActivity(), "No puede agregar mas bebidas!", Toast.LENGTH_SHORT).show();
                                }
                                dialog.dismiss();

                            }

                        });

                    }
                }
        );





        return v;


    }

    public void fuente(){

        //Instanciamos textview el titulo
        titulo = (TextView)v.findViewById(R.id.textViewTitulo);
        titulo.setTypeface(EasyFonts.captureIt(getActivity()));

    }



}
