package com.tesicohlabs.drinkapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.VideoView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.vstechlab.easyfonts.EasyFonts;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class datos2 extends AppCompatActivity {
    Toolbar toolbar;
    Typeface typeface;
    String nombre;

    DrinkAppBD bd;
    SQLiteDatabase db;

    boolean entrar;
    MultiAutoCompleteTextView multiAutoCompleteTextView;


    //Importamos la clase date y jugamos con esto baby
    int horaF;

    Thread timerThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos2);

        entrar = true;

        bd = new DrinkAppBD(this,"DrinkAppBD",null,1);
        db = bd.getWritableDatabase();

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        //Spinner viejaa

        Cursor c = db.rawQuery(" SELECT * FROM medicamentos ORDER BY nombre ASC", null);


        int i =1;

        String[] SPINNERLIST = new String[123];
        SPINNERLIST[0] = "Ninguno";

        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                SPINNERLIST[i] = c.getString(0);
                i++;
            } while(c.moveToNext());
        }

        db.close();
        c.close();


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST);

        String[] SPINNERLIST2 = {"Frecuente","Moderado","Casual","Fines de Semana","Todos los dias"};
        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST2);

        MaterialBetterSpinner materialTomadorFre = (MaterialBetterSpinner)findViewById(R.id.android_material_design_spinner2);
        materialTomadorFre.setAdapter(arrayAdapter2);

        multiAutoCompleteTextView = (MultiAutoCompleteTextView) findViewById(R.id.multi_auto_complete_textview);
        multiAutoCompleteTextView.setAdapter(arrayAdapter);
        multiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        multiAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
            }
        });


        Date date = new Date();
        DateFormat hourFormat = new SimpleDateFormat("HHmm");
        String hora = hourFormat.format(date);
        horaF = Integer.parseInt(hora);
        basePrograma();

    }


    private void basePrograma(){
        //Creamos la base de datos si no esta creada
        DrinkAppBD bd = new DrinkAppBD(this,"DrinkAppBD",null,1);
        SQLiteDatabase db = bd.getWritableDatabase();

        Cursor c = db.rawQuery(" SELECT * FROM Persona", null);
        if (c.moveToFirst()) {
            nombre = c.getString(0);
        }
        db.close();
        c.close();
        TextView titulo = (TextView)findViewById(R.id.textView12);
        titulo.setText("Vas a tomar ?");
        titulo.setTextSize(25);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/warning.ttf");
        titulo.setTypeface(typeface);

        CheckBox comio = (CheckBox) findViewById(R.id.checkBox2);
        comio.setTypeface(EasyFonts.robotoRegular(this));

        if(horaF>2000 & horaF<=2359){
            comio.setHint(" Ya Cenaste?");
        }else if(horaF>0630 & horaF<=1100){
            comio.setHint(" Ya Desayunaste?");
        }else if(horaF>1100 & horaF<=1730){
            comio.setHint(" Ya Almorzaste?");
        }else if (horaF>1730 & horaF<=2000){
            comio.setHint(" Ya Merendaste?");
        }else if(horaF>=0000 & horaF<=0630){
            comio.setHint(" Ya Cenaste?");
        }
    }

    public void btnComenzar(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        final VideoView videoview = (VideoView) dialog.findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video);
        videoview.setVideoURI(uri);
        videoview.start();
        videoview.requestFocus();

        final FloatingActionButton stop = (FloatingActionButton)dialog.findViewById(R.id.botonPuto);

//        final Button stop = (Button)dialog.findViewById(R.id.btnStop);

      stop.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        videoview.stopPlayback();
                        dialog.dismiss();
                        entrar = false;
                        close();
                    }
                });

        dialog.show();

    }

    public void close(){
        Intent intent = new Intent(this, datosVariables.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        YoYo.with(Techniques.FadeOut)
                .duration(100)
                .playOn(findViewById(R.id.datos2Layout));


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Datos.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        YoYo.with(Techniques.RotateIn)
                .duration(500)
                .playOn(findViewById(R.id.datos2Layout));
    }

    @Override
    public void onPause(){
        super.onPause();
    }


}
